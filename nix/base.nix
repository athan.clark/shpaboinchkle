build-or-shell:
{ chan ? "5272327b81ed355bbed5659b8d303cf2979b6953"
, compiler ? "ghc865"
, withHoogle ? false
, doHoogle ? false
, doHaddock ? false
, enableLibraryProfiling ? false
, enableExecutableProfiling ? false
, strictDeps ? false
, isJS ? false
, asShell ? false
, system ? builtins.currentSystem
, optimize ? true
, user ? "fresheyeball"
, branch ? "master"
, commit ? null
, shpadoinkle
    ? builtins.fetchGit ({
        url = "https://gitlab.com/${user}/Shpadoinkle.git";
        ref = branch;
      } // (if commit != null then { rev = commit; } else {}))
}:
let
  # Additional ignore patterns to keep the Nix src clean
  ignorance = [
    "*.md"
    "figlet"
    "*.nix"
    "*.sh"
    "*.yml"
  ];


  # Get some utilities
  basePkgs = import (shpadoinkle + "/nix/base-pkgs.nix") { inherit chan; } {};
  inherit (import (shpadoinkle + "/nix/util.nix") { pkgs = basePkgs; inherit compiler isJS; }) compilerjs gitignore doCannibalize;


  # Build faster by doing less
  chill = p: (pkgs.haskell.lib.overrideCabal p {
    inherit enableLibraryProfiling enableExecutableProfiling;
  }).overrideAttrs (_: {
    inherit doHoogle doHaddock strictDeps;
  });


  # Overlay containing Shpadoinkle packages, and needed alterations for those packages
  # as well as optimizations from Reflex Platform
  shpadoinkle-overlay =
    import (shpadoinkle + "/nix/overlay.nix") { inherit chan compiler isJS enableLibraryProfiling enableExecutableProfiling; };


  # Haskell specific overlay (for you to extend)
  haskell-overlay = hself: hsuper: {
    "happy" = pkgs.haskell.lib.dontCheck hsuper.happy;
  };


  # Top level overlay (for you to extend)
  shpaboinchkle-overlay = self: super: {
    haskell = super.haskell //
      { packages = super.haskell.packages //
        { ${compilerjs} = super.haskell.packages.${compilerjs}.override (old: {
            overrides = super.lib.composeExtensions (old.overrides or (_: _: {})) haskell-overlay;
          });
        };
      };
    };


  # Complete package set with overlays applied
  pkgs = import
    (builtins.fetchTarball {
      url = "https://github.com/NixOS/nixpkgs/archive/${chan}.tar.gz";
    }) {
    inherit system;
    overlays = [
      shpadoinkle-overlay
      shpaboinchkle-overlay
    ];
  };


  ghcTools = with pkgs.haskell.packages.${compiler};
    [ cabal-install
      ghcid
    ] ++ (if isJS then [] else [ stylish-haskell ]);


  # We can name him George
  shpaboinchkle = pkgs.haskell.packages.${compilerjs}.callCabal2nix "shpaboinchkle" (gitignore ignorance ../haskell) {};


in with pkgs; with lib;
  if build-or-shell == "build" then
    (if isJS && optimize then doCannibalize else x: x) (chill shpaboinchkle)
  else if build-or-shell == "shell" then
    pkgs.haskell.packages.${compilerjs}.shellFor
      { inherit withHoogle;
        packages = _: [ shpaboinchkle ];
        COMPILER = compilerjs;
        buildInputs = ghcTools;

        shellHook =
          ''
          {lolcat}/bin/lolcat ${./figlet}
          cat ${./intro}
          '';
      }
  else
      builtins.throw ''The first argument to this function must be either "build" or "shell"''
